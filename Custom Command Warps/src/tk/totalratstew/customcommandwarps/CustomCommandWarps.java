package tk.totalratstew.customcommandwarps;

import java.util.List;

import org.bukkit.plugin.java.JavaPlugin;

public final class CustomCommandWarps extends JavaPlugin {
	public void onEnable(){
		this.saveDefaultConfig();
		this.saveConfig();
		List<String> tpnames = this.getConfig().getStringList("TpNames");
		if (tpnames.isEmpty()){
			getLogger().info("CCW found no warps");
		}
		else{
		for(int i=0; i<tpnames.size();){
		getCommand(tpnames.get(i)).setExecutor(new TpCmd(this,tpnames.get(i)));
		i++;
		}
		}
		getCommand("ccw").setExecutor(new CCWCmd(this));
	}
}