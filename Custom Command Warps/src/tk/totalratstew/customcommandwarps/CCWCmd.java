package tk.totalratstew.customcommandwarps;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
//import org.bukkit.plugin.java.JavaPlugin;

public class CCWCmd implements CommandExecutor { 
	
	private CustomCommandWarps plugin;
	
	public CCWCmd(CustomCommandWarps plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("ccw")){
			if(args[0].equalsIgnoreCase("set")){
				Player player = (Player) sender;
				List<String> temptpnames = plugin.getConfig().getStringList("TpNames");
				temptpnames.add(args[1]);
				plugin.getConfig().set("TpNames", temptpnames);
				plugin.getConfig().set("Location." + args[1] + ".world", player.getLocation().getWorld().getName());
				plugin.getConfig().set("Location." + args[1] + ".x", player.getLocation().getX());
				plugin.getConfig().set("Location." + args[1] + ".y", player.getLocation().getY());
				plugin.getConfig().set("Location." + args[1] + ".z", player.getLocation().getZ());
				plugin.getConfig().set("Location." + args[1] + ".pitch", player.getLocation().getPitch());
				plugin.getConfig().set("Location." + args[1] + ".yaw", player.getLocation().getYaw());
				plugin.saveConfig();
				player.sendMessage("" + args[1] + " set!");
				return true;
				}
		}
		return false;
	}
}
