package tk.totalratstew.customcommandwarps;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TpCmd implements CommandExecutor { 
	
	private CustomCommandWarps plugin;
	private String cmdname;
	
	public TpCmd(CustomCommandWarps plugin, String cmdname) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase(cmdname)){
			Player player = (Player) sender;
			World world = player.getWorld();
			if (plugin.getConfig().getBoolean("Settings.Effects.Explosions")){
				world.createExplosion(player.getLocation(), 0);}
			if (plugin.getConfig().getBoolean("Settings.Effects.Lightning")){
				world.strikeLightningEffect(player.getLocation());}
			World tpworld = Bukkit.getServer().getWorld(plugin.getConfig().getString("Location."+cmdname+".world"));
			double tpx = Double.parseDouble(plugin.getConfig().getString("Location."+cmdname+".x"));
			double tpy = Double.parseDouble(plugin.getConfig().getString("Location."+cmdname+".y"));
			double tpz = Double.parseDouble(plugin.getConfig().getString("Location."+cmdname+".z"));
			float tppitch = Float.parseFloat(plugin.getConfig().getString("Location."+cmdname+".pitch"));
			float tpyaw = Float.parseFloat(plugin.getConfig().getString("Location."+cmdname+".yaw"));
			Location tplocation = player.getLocation();			
			tplocation.setWorld(tpworld);
			tplocation.setX(tpx);
			tplocation.setY(tpy);
			tplocation.setZ(tpz);
			tplocation.setPitch(tppitch);
			tplocation.setYaw(tpyaw);
			Bukkit.getServer().createWorld(new WorldCreator(plugin.getConfig().getString("Location."+cmdname+".world")));
			player.sendMessage("Loaded!");
			player.teleport(tplocation);
			player.sendMessage("Spawn!");
			return true;
		}
		return false;
	}
}
